﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UserControl : MonoBehaviour {

    public Transform shapeTrans;
    public float rotSpeed;
    public float rotFactor = 0.01f;

    public float wantedZRot;
    public float currZRot;
    public float rotateAmount = 0f;
    public float rotZVel;

    public bool isLeftPressed, isRightPressed;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Rotation();
        shapeTrans.rotation = Quaternion.Euler(0, 0 , currZRot);
    }

    #region Utility Methods

    void Rotation()
    { 
        wantedZRot += rotateAmount;
        
        if(isLeftPressed)
        {
            rotateAmount += rotFactor;
        }
        else if(isRightPressed)
        {
            rotateAmount -= rotFactor;
        }
        else if(!isLeftPressed && !isRightPressed)
        {
            if(rotateAmount > 0)
            {
                print("IF");
                rotateAmount -= rotFactor/2f;
            }                
            else if(rotateAmount < 0)
            {
                print("ELSE IF");
                rotateAmount += rotFactor/2f;
            }                
        }

        currZRot = Mathf.SmoothDamp(currZRot, wantedZRot, ref rotZVel, rotSpeed * Time.deltaTime);
    }

    #endregion

    #region UI Button Events
    
    public void LeftArrowBtn(bool isPressed)
    {
        if (isPressed)
        {            
            isLeftPressed = true;
            isRightPressed = false;
        }
        else
        {           
            isLeftPressed = isRightPressed = false;
        }
    }

    public void RightArrowBtn(bool isPressed)
    {
        if (isPressed)
        {           
            isLeftPressed = false;
            isRightPressed = true;
        }
        else
        {            
            isLeftPressed = isRightPressed = false;
        }
    }

    #endregion
}
